<?php

/* enqueue scripts and style from parent theme */
function twentytwentyone_styles()
{
    wp_enqueue_style(
        'child-style',
        get_stylesheet_uri(),
        ['twenty-twenty-one-style'],
        wp_get_theme()->get('Version')
    );
}
add_action('wp_enqueue_scripts', 'twentytwentyone_styles');

function add_theme_scripts()
{
    // enqueue load more css
    wp_enqueue_style('loadmore-button', get_stylesheet_directory_uri() . '/assets/css/loadmore-button.css', false, false, 'all');

    // load more scripts
    global $wp_query;
    // register our main script but do not enqueue it yet
    wp_register_script('my_loadmore', get_stylesheet_directory_uri() . '/assets/js/myloadmore.js', array('jquery'));
    // pass parameters to myloadmore.js script
    wp_localize_script(
        'my_loadmore', 'misha_loadmore_params', array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'posts' => json_encode($wp_query->query_vars), // everything about your loop is here
            'current_page' => get_query_var('paged') ? get_query_var('paged') : 1,
            'max_page' => $wp_query->max_num_pages
        )
    );
    wp_enqueue_script('my_loadmore');
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');

class product_post_type
{
    /** resgister post type product */
    function registerProductPostType()
    {
        $labels = array(
            'name'               => _x('Products', 'post type general name'),
            'singular_name'      => _x('Product', 'post type singular name'),
            'add_new'            => _x('Add New', 'book'),
            'add_new_item'       => __('Add New Product'),
            'edit_item'          => __('Edit Product'),
            'new_item'           => __('New Product'),
            'all_items'          => __('All Products'),
            'view_item'          => __('View Product'),
            'search_items'       => __('Search Products'),
            'not_found'          => __('No products found'),
            'not_found_in_trash' => __('No products found in the Trash'),
            'parent_item_colon'  => '',
            'menu_name'          => 'Products'
        );
        $args = array(
            'labels'        => $labels,
            'description'   => 'Holds our products and product specific data',
            'public'        => true,
            'menu_position' => 5,
            'supports'      => array('title', 'editor', 'thumbnail', 'excerpt', 'comments'),
            'has_archive'   => true,
            'show_in_rest'  => true,
        );
        register_post_type('product', $args);
    }

    /**
     * update message for product post type 
     *
     * @param array $messages
     * @return array
     */
    function productPostTypeUpdatedMessages($messages)
    {
        global $post, $post_ID;
        $messages['product'] = array(
            0 => '',
            1 => sprintf(__('Product updated. <a href="%s">View product</a>'), esc_url(get_permalink($post_ID))),
            2 => __('Custom field updated.'),
            3 => __('Custom field deleted.'),
            4 => __('Product updated.'),
            5 => isset($_GET['revision']) ? sprintf(__('Product restored to revision from %s'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
            6 => sprintf(__('Product published. <a href="%s">View product</a>'), esc_url(get_permalink($post_ID))),
            7 => __('Product saved.'),
            8 => sprintf(__('Product submitted. <a target="_blank" href="%s">Preview product</a>'), esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
            9 => sprintf(__('Product scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview product</a>'), date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url(get_permalink($post_ID))),
            10 => sprintf(__('Product draft updated. <a target="_blank" href="%s">Preview product</a>'), esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
        );
        return $messages;
    }

    /**
     * Contextual help for product post type
     *
     */
    function productPostTypeContextualHelp()
    {
        $screen = get_current_screen();
        if ('product' == $screen->id) {
            $screen->add_help_tab([
                'id' => 'products',
                'title' => 'Overview',
                'content' => '<h2>Products</h2>
          <p>Products show the details of the items that we sell on the website. You can see a list of them on this page in reverse chronological order - the latest one we added is first.</p> 
          <p>You can view/edit the details of each product by clicking on its name, or you can perform bulk actions using the dropdown menu and selecting multiple items.</p>'
            ]);
        }
        if ('edit-product' == $screen->id) {
            $screen->add_help_tab([
                'id' => 'editing_products',
                'title' => 'Overview',
                'content' => '<h2>Editing Products</h2>
          <p>This page allows you to view/modify product details. Please make sure to fill out the available boxes with the appropriate details (product image, price, brand) and <strong>not</strong> add these details to the product description.</p>'
            ]);
        }
    }

    /**
     * add category taxonomy to product post type
     *
     */
    function registerProductCategoryTaxonomy()
    {
        $labels = array(
            'name'              => _x('Product Categories', 'taxonomy general name'),
            'singular_name'     => _x('Product Category', 'taxonomy singular name'),
            'search_items'      => __('Search Product Categories'),
            'all_items'         => __('All Product Categories'),
            'parent_item'       => __('Parent Product Category'),
            'parent_item_colon' => __('Parent Product Category:'),
            'edit_item'         => __('Edit Product Category'),
            'update_item'       => __('Update Product Category'),
            'add_new_item'      => __('Add New Product Category'),
            'new_item_name'     => __('New Product Category'),
            'menu_name'         => __('Product Categories'),
        );
        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
        );
        register_taxonomy('product_category', 'product', $args);
    }

    /**
     * add price meta box for product post type
     *
     */
    function productPriceMetabox()
    {
        add_meta_box(
            'product_price_box',
            __('Product Price'),
            [$this, 'renderProductPriceMetabox'],
            'product',
            'side',
            'high'
        );
    }

    /**
     * get method for product price meta
     *
     * @param int $productId
     */
    function getProductPrice($productId = null)
    {
        global $post;
        if (empty($productId)) {
            $productId = $post->ID;
        }
        return esc_attr(get_post_meta($productId, 'product_price', true));
    }
    /**
     * renderer for product price meta box
     *
     * @param [type] $product
     */
    function renderProductPriceMetabox($product)
    {
        $productPrice = $this->getProductPrice();
        wp_nonce_field('_product_price_box_content_nonce', 'product_price_box_content_nonce');
        echo '<label for="product_price"></label>';
        echo '<input type="text" id="product_price" name="product_price" placeholder="enter a price" value="' . $productPrice . '" />';
    }

    /**
     * save action for product price meta
     *
     * @param int $productId
     */
    function saveProductPrice($productId)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
        if (!wp_verify_nonce($_POST['product_price_box_content_nonce'], '_product_price_box_content_nonce')) {
            return;
        }
        if (!current_user_can('edit_post', $productId)) {
            return;
        }
        $productPrice = floatval($_POST['product_price']);
        update_post_meta($productId, 'product_price', $productPrice);
    }

    /**
     * add color meta box for product post type
     *
     */
    function productColorMetabox()
    {
        add_meta_box(
            'product_color_box',
            __('Product Color'),
            [$this, 'renderProductColorMetabox'],
            'product',
            'side',
            'high'
        );
    }

    /**
     * get method for product color meta
     *
     * @param int $productId
     */
    function getProductColor($productId = null)
    {
        global $post;
        if (empty($productId)) {
            $productId = $post->ID;
        }
        return esc_attr(get_post_meta($productId, 'product_color', true));
    }
    /**
     * renderer for product color meta box
     *
     * @param [type] $product
     */
    function renderProductColorMetabox($product)
    {
        $productColor = $this->getProductColor();
        wp_nonce_field('_product_color_box_content_nonce', 'product_color_box_content_nonce');
        echo '<label for="product_color"></label>';
        echo '<select name="product_color" id="product_color">';
        echo '<option value="">Select a color</option>';
        foreach (['red', 'blue', 'yellow'] as $value) {
            echo '<option value="' . $value . '" ' . ($productColor == $value ? "selected" : "") . '>' . ucfirst($value) . '</option>';
        }
        echo '</select>';
    }

    /**
     * save action for product color meta
     *
     * @param int $productId
     */
    function saveProductColor($productId)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
        if (!wp_verify_nonce($_POST['product_color_box_content_nonce'], '_product_color_box_content_nonce')) {
            return;
        }
        if (!current_user_can('edit_post', $productId)) {
            return;
        }
        $productColor = $_POST['product_color'];
        update_post_meta($productId, 'product_color', $productColor);
    }

    /**
     * add shippable meta box for product post type
     *
     */
    function productShippableMetabox()
    {
        add_meta_box(
            'product_shippable_box',
            __('Product Shippable'),
            [$this, 'renderProductShippableMetabox'],
            'product',
            'side',
            'high'
        );
    }

    /**
     * get method for product shippable meta
     *
     * @param int $productId
     */
    function getProductShippable($productId = null)
    {
        global $post;
        if (empty($productId)) {
            $productId = $post->ID;
        }
        return esc_attr(get_post_meta($productId, 'product_shippable', true));
    }
    /**
     * renderer for product shippable meta box
     *
     * @param [type] $product
     */
    function renderProductShippableMetabox($product)
    {
        $productShippable = $this->getProductShippable();
        wp_nonce_field('_product_shippable_box_content_nonce', 'product_shippable_box_content_nonce');
        echo '<label for="product_shippable"></label>';
        echo '<input type="checkbox" id="product_shippable" name="product_shippable" value="shippable" ' . ($productShippable ? "checked" : "") . '>';
    }

    /**
     * save action for product shippable meta
     *
     * @param int $productId
     */
    function saveProductShippable($productId)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
        if (!wp_verify_nonce($_POST['product_shippable_box_content_nonce'], '_product_shippable_box_content_nonce')) {
            return;
        }
        if (!current_user_can('edit_post', $productId)) {
            return;
        }
        $productShippable = boolval($_POST['product_shippable']);
        update_post_meta($productId, 'product_shippable', $productShippable);
    }

    /**
     * add company meta box for product post type
     *
     */
    function productCompanyMetabox()
    {
        add_meta_box(
            'product_company_box',
            __('Product Company'),
            [$this, 'renderProductCompanyMetabox'],
            'product',
            'side',
            'high'
        );
    }

    /**
     * get method for product company meta
     *
     * @param int $productId
     */
    function getProductCompany($productId = null)
    {
        global $post;
        if (empty($productId)) {
            $productId = $post->ID;
        }
        return esc_attr(get_post_meta($productId, 'product_company', true));
    }
    /**
     * renderer for product company meta box
     *
     * @param [type] $product
     */
    function renderProductCompanyMetabox($product)
    {
        $productCompany = $this->getProductCompany();
        wp_nonce_field('_product_company_box_content_nonce', 'product_company_box_content_nonce');
        foreach (['nike', 'adidas'] as $value) {
            echo '<label for="product_company_' . $value . '">' . ucfirst($value) . '</label>';
            echo '<input type="radio" id="product_company_' . $value . '" name="product_company" value="' . $value . '" ' . ($value == $productCompany ? "checked" : "") . '>';
        }
    }

    /**
     * save action for product company meta
     *
     * @param int $productId
     */
    function saveProductCompany($productId)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
        if (!wp_verify_nonce($_POST['product_company_box_content_nonce'], '_product_company_box_content_nonce')) {
            return;
        }
        if (!current_user_can('edit_post', $productId)) {
            return;
        }
        $productCompany = $_POST['product_company'];
        update_post_meta($productId, 'product_company', $productCompany);
    }
}

$product = new product_post_type();
add_action('init', [$product, 'registerProductPostType']);
add_filter('post_updated_messages', [$product, 'productPostTypeUpdatedMessages']);
add_action('admin_head', [$product, 'productPostTypeContextualHelp']);
add_action('init', [$product, 'registerProductCategoryTaxonomy'], 0);

// price meta
add_action('add_meta_boxes', [$product, 'productPriceMetabox']);
// save handler for price meta box
add_action('save_post_product', [$product, 'saveProductPrice']);

// color meta
add_action('add_meta_boxes', [$product, 'productColorMetabox']);
// save handler for color meta box
add_action('save_post_product', [$product, 'saveProductColor']);

// shippable meta
add_action('add_meta_boxes', [$product, 'productShippableMetabox']);
add_action('save_post_product', [$product, 'saveProductShippable']);

// company meta
add_action('add_meta_boxes', [$product, 'productCompanyMetabox']);
add_action('save_post_product', [$product, 'saveProductCompany']);

// [mnet_ad] mnet ad javascript shortcode
function mnet_ad_func()
{
    ob_start();
?>
    <script id="mNCC" language="javascript">
        medianet_width = "800";
        medianet_height = "250";
        medianet_crid = "185897927";
        medianet_versionId = "3111299";
    </script>
    <script src="https://contextual.media.net/nmedianet.js?cid=8CUY82DKD"></script>
<?php
    return ob_get_clean();
}
add_shortcode('mnet_ad', 'mnet_ad_func');

// add sort order by title
add_action('pre_get_posts', function ($query) {
    if (is_category() && $query->is_main_query()) {
        $query->set('orderby', 'title');
        $query->set('order', 'asc');
        return $query;
    }
});

// [products_names ids="comma separated product ids"]
function get_product_names($atts)
{
    $a = shortcode_atts([
        'ids' => []
    ], $atts);

    if ($a['ids'] == [])
        return "";
    else
        $a['ids'] = explode(",", $a['ids']);

    $args = [
        'post_type' => 'product',
        'post__in' => $a['ids']
    ];

    $query = new WP_Query($args);

    if (!$query->have_posts())
        return "";

    ob_start();
?>
    <ul>
        <?php while ($query->have_posts()) : $query->the_post(); ?>
            <li><?php the_title(); ?></li>
        <?php endwhile ?>
    </ul>
<?php
    return ob_get_clean();
}
add_shortcode('products_names', 'get_product_names');

function premiumProductsRSS()
{
    add_feed('premium_products', 'premiumProductsRSSFunc');
}
function premiumProductsRSSFunc()
{
    get_template_part('rss', 'premium_products');
}
add_action('init', 'premiumProductsRSS');

// premium products api 
add_action('rest_api_init', function () {
    register_rest_route(get_stylesheet() . '/v1', '/premium_products/', [
        'methods' => 'GET',
        'callback' => 'premiumProductsApiFunc',
        'permission_callback' => '__return_true',
    ]);
});

function premiumProductsApiFunc()
{
    $premiumProducts = [];
    $args = [
        'post_type' => 'product',
        'meta_key' => 'product_price',
        'meta_query' => [
            [
                'key' => 'product_price',
                'value' => 500,
                'compare' => '>='
            ]
        ]
    ];
    $premium_products = new WP_Query($args);
    while ($premium_products->have_posts()) : $premium_products->the_post();
        $premiumProducts[] = [
            'id' => get_the_ID(),
            'title' => get_the_title(),
            'link' => get_the_permalink(),
            'description' => get_the_excerpt(),
            'price' => get_post_meta(get_the_ID(), 'product_price', true)
        ];
    endwhile;
    return $premiumProducts;
}

// load more button
// css and js related to load more loaded in enqueue above
function misha_loadmore_ajax_handler()
{
    // prepare arguments for the query
    $args = json_decode(stripslashes($_POST['query']), true);
    $args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
    $args['post_status'] = 'publish';
    // it is always better to use WP_Query but not here
    query_posts($args);
    if (have_posts()) :
        while (have_posts()) : the_post();
            get_template_part('template-parts/content/content', get_theme_mod('display_excerpt_or_full_post', 'excerpt'));
        endwhile;
    endif;
    die;
}
add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler');
add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler');

// setting page
// function plugin_settings_page_content() {
//     echo 'Feeds!';
// }
// function create_plugin_settings_page() {
//     $page_title = 'Feed Settings Page';
//     $menu_title = 'Feed Settings';
//     $capability = 'manage_options';
//     $slug = '_fields';
//     $callback = 'plugin_settings_page_content';
//     $icon = 'dashicons-admin-plugins';
//     $position = 100;
//     // add_menu_page( $page_title, $menu_title, $capability, $slug, $callback, $icon, $position );
//     add_submenu_page( 'options-general.php', $page_title, $menu_title, $capability, $slug, $callback );
// }
// add_action( 'admin_menu', 'create_plugin_settings_page');


// add setting page using acf pro
if (function_exists('acf_add_options_page')) {
    acf_add_options_page([
        'page_title' 	=> 'Custom Settings',
		'menu_title'	=> 'Custom Settings',
		'menu_slug' 	=> 'avi-custom-settings',
		'capability'	=> 'manage_options',
		'redirect'		=> false
    ]);
    acf_add_options_sub_page([
        'page_title' 	=> 'Feeds Settings',
		'menu_title'	=> 'Feeds',
		'parent_slug'	=> 'avi-custom-settings',
    ]);
}

// create block usig acf pro
add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types() {
	if( function_exists('acf_register_block_type') ) {
		// register a testimonial block.
		acf_register_block_type(array(
			'name'				=> 'testimonial',
			'title'				=> __('Testimonial'),
			'description'		=> __('A custom testimonial block.'),
			'render_template'	=> 'template-parts/blocks/testimonial/testimonial.php',
			'category'			=> 'formatting',
			'icon'				=> 'admin-comments',
			'keywords'			=> array( 'testimonial', 'quote' ),
		));
	}
}

// check template and save transient
add_filter( 'template_include', 'check_valid_template', 99 );
function check_valid_template($template) {
    if (false === get_transient($template . "template_exists")) {
        // It wasn't there, so regenerate the data and save the transient
        $template_exist = empty(locate_template($template)) ? "not found" : "exist";
        set_transient( $template . "template_exists", $template_exist, 12 * HOUR_IN_SECONDS );
    }
    else {
        echo $template . " " . get_transient($template . "template_exists");
    }
    return $template;
}

/**
 * Prints last post details, command : last-published-post
 * 
 */
function lastPublishedPostFunc()
{
    // $posts = wp_get_recent_posts([], 'OBJECT');
    $posts = wp_get_recent_posts();
    if (!empty($posts)) {
        WP_CLI::log( 'ID :' . $posts[0]['ID']);
        WP_CLI::log( 'Title :' . $posts[0]['post_title']);
        WP_CLI::log( 'Content :' . $posts[0]['post_content']);
        WP_CLI::log( 'Template :' . get_page_template_slug($posts[0]['ID']));
        WP_CLI::success('last-published-post run successfully');
    } elseif ($post !== false) {
        WP_CLI::success('No post found');
    } else {
        WP_CLI::warning('Something went wrong');
    }
}
if ( class_exists( 'WP_CLI' ) ) {
    WP_CLI::add_command( 'last-published-post', 'lastPublishedPostFunc' );
}