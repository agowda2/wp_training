<h3>Feeds</h3>
<ul class="feeds">
    <li><a href="<?php echo get_feed_link('premium_products'); ?>" title="<?php _e('Premium Products'); ?>"><?php _e('Premium Products'); ?></a></li>
    <li><a href="<?php bloginfo('rss2_url'); ?>" title="<?php _e('Syndicate this site using RSS'); ?>"><?php _e('<abbr title="Really Simple Syndication">RSS</abbr>'); ?></a></li>
</ul>