<div class="premium_product-list">
<?php 
    $response = wp_remote_get( 'http://localhost/wp_practice/wp-json/wptraining/v1/premium_products/' );
    if (wp_remote_retrieve_response_code($response) === 200) :
        $premium_products = json_decode(wp_remote_retrieve_body($response), true);
        if (!empty($premium_products) && is_array($premium_products)) :
?>
    <h3><?= _e('Premium Products') ?></h3>
    <ul>
    <?php foreach ($premium_products as $product) : ?>
        <li>
            <a  href="<?= $product['link'] ?>"><?= _e($product['title']) . ' ( $ ' . $product['price'] . ')' ?></a>
        </li>
    <?php endforeach; ?>
    </ul>
<?php
        endif;
    endif;
?>
</div>